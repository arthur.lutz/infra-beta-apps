# infra-beta-apps
**Ce projet temporaire a été mis en place dans le cadre du confinement (COVID) et ne sera continué - L'architecture cible de APPS-EDUCATION est basée sur Kubernetes**

Docker-compose pour les services BETA-APPS

Par services :
- service.yml
- service-dist.env


Pour voir la configuration par defaut:
docker-compose  -f service.yml config  
ex docker-compose  -f  auth.yml   config


Pour modifier les valeurs
Copier le service-dist.env sur service.env
Adapter le fichier service.env

Pour voir la configuration qui sera appliquée
docker-compose  -f service.yml  -env-file service.env config  

Pour lancer les conteneurs:
docker-compose  -f service.yml  -env-file service.env up

Le reverse proxy traefik est configuré par les fichiers présent dans le dossier traefik

Liste des services:

  Techniques:
  - traefik       : Reverse Proxy
  - portainer     : Administration Docker
  - mongo-express : Gestion Base Données

  Utilisateurs:
  - keycloak : Getion d'identité
  - laboite  : Portail de services
  - etherpad : Pad collaboratif
  - codiMD   : Pad collaboratif en MarkDown
